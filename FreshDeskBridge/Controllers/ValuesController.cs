﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FreshDeskBridge.Models;
using FreshDeskBridge.Repositories;

namespace FreshDeskBridge.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {

        private readonly ISynchRepository _synchronize;
        public ValuesController(ISynchRepository synchronize)
        {
            _synchronize = synchronize;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Ticket ticket)
        {
            if (ticket == null)
                return BadRequest();

            var result = await _synchronize.SynchronizeAsync(ticket);
            return Ok(
                result
            );
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
