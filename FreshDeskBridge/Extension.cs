﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreshDeskBridge
{
    public static  class Extension
    {
        public static string GetIDGitLabIssue(this string title)
        {
            return title.Split('-').FirstOrDefault();
        }
    }
}
