﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreshDeskBridge.Models
{
    public class GitLabIssue
    {
        public int id { get; set; }
        public int iid { get; set; }
        public int project_id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string state { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public List<string> labels { get; set; }
        public object milestone { get; set; }
        public List<object> assignees { get; set; }
        public Author author { get; set; }
        public object assignee { get; set; }
        public int user_notes_count { get; set; }
        public int upvotes { get; set; }
        public int downvotes { get; set; }
        public object due_date { get; set; }
        public bool confidential { get; set; }
        public string web_url { get; set; }
    }

    public class Author
    {
        public string name { get; set; }
        public string username { get; set; }
        public int id { get; set; }
        public string state { get; set; }
        public string avatar_url { get; set; }
        public string web_url { get; set; }
    }

   
}
